#include "functions.h"

const char* ssid = "";
const char* password = "";

WiFiServer server(80);

String serverName = "http://192.168.1.27:80/";
unsigned long lastTime = 0;
// Set timer to 5 seconds (5000)
//unsigned long timerDelay = 5000;
unsigned long timerDelay = 900000;

IPAddress ip;
float temp = 0;
float hum = 0;
//int var = 0;  // value read from the pot
char men_head[500];
char men_temp[100];
char men_hum[100];
//char men_led[500];
char men_red[200];
char men_blue[200];
char men_green[200];

void setup() {
  Serial.begin(115200);
  delay(10);

  //Configuration of A0 for temperature reading
  //pinMode(A0, INPUT);   
  dht.begin();             //Iniciamos el sensor
  //Configuration of GPIO14 for color Red
  pinMode(14, OUTPUT);
  digitalWrite(14,LOW);
  //Configuration of GPIO12 for color Green
  pinMode(12, OUTPUT);
  digitalWrite(12,LOW);
  //Configuration  del GPIO13 for color Blue
  pinMode(13, OUTPUT);
  digitalWrite(13,LOW);
  
  
  Serial.println();
  Serial.println();
  Serial.print("Conectandose a red : ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password); //Network connection
  
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi conectado");
  
  
  server.begin(); //Start the server
  Serial.println("Servidor Iniciado");


  Serial.println("Ingrese desde un navegador web usando la siguiente IP:");
  Serial.println(WiFi.localIP()); //Obtain the IP address
  Serial.println();
  ip = WiFi.localIP();
}

void loop() {

  temp_hum();

  servidor();

  clienteWeb();
  
}
