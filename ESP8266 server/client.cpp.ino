#include "functions.h"

void clienteWeb(void){
  if ((millis() - lastTime) > timerDelay) {
    int counter=0;
    //Check WiFi connection status
    if(WiFi.status()== WL_CONNECTED){
      WiFiClient client;
      HTTPClient http;

      String serverPath = serverName + "datos.html";
      
      // Your Domain name with URL path or IP address with path
      http.begin(client, serverPath.c_str());
      http.addHeader("Referer", "datos.html");
      // Data to send with HTTP POST
      char httpRequestData[500];
      sprintf(httpRequestData, "api_key=tPmAT5Ab3j7F9&sensor=DHT11&value=%0.2f&magnitud=temperatura&value=%0.2f&magnitud=humedad", temp, hum);
      
      int httpResponseCode;
      do{
        // Send HTTP POST request
        http.addHeader("Referer", "datos.html");
        httpResponseCode = http.POST(httpRequestData);
        
        if (httpResponseCode==200) {
          Serial.print("HTTP Response code: ");
          Serial.println(httpResponseCode);
        }
        else {
          Serial.print("Error code: ");
          Serial.println(httpResponseCode);
        }
        counter++;
      }while(httpResponseCode!=200 && counter<10);
      
      // Free resources
      http.end();
    }
    else {
      Serial.println("WiFi Disconnected");
    }
    lastTime = millis();
  }
}
