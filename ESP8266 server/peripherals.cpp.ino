#include "functions.h"

void temp_hum(void){
  // read the analog in value
  //var = analogRead(A0);
  //temp = var*0.0032258/0.01;

  // Leemos la humedad relativa
  hum = dht.readHumidity(); 
  // Leemos la temperatura en grados centígrados (por defecto)
  temp = dht.readTemperature();
 
  // Comprobamos si ha habido algún error en la lectura
  if (isnan(hum) || isnan(temp) ){
    Serial.println("Error obteniendo los datos del sensor DHT11");
    return;
  }
  
  if (temp>27.5) {
    sprintf(men_temp, "La temperatura es: <br> <b><font color='RED'>%.2f \xB0""C</font></b>", temp);
  } else if (temp<22) {
    sprintf(men_temp, "La temperatura es: <br> <b><font color='BLUE'>%.2f \xB0""C</font></b>", temp);
  } else {
    sprintf(men_temp, "La temperatura es: <br> <b><font color='GREEN'>%.2f \xB0""C</font></b>", temp);
  }
  char percent[10] = "%";
  sprintf(men_hum, "<br><br><br>La humedad es: <br> <b>%.2f%s </b>", hum, percent);
  
  delay(5);
}
