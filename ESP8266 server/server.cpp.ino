#include "functions.h"

void servidor(void){ 
  WiFiClient client = server.available();
  if (client) //Si hay un cliente presente
  { 
    
    //esperamos hasta que hayan datos disponibles
    while(!client.available()&&client.connected())
    {
    delay(1);
    }
    
    // Leemos la primera línea de la petición del cliente.
    String linea1 = client.readStringUntil(' H');
    Serial.println(linea1);

    /*if (linea1.indexOf("LED=ON")>0) { //Buscamos un LED=ON en la 1°Linea
      digitalWrite(2,HIGH);
      sprintf(men_led, "<form method='GET' action=LED> <input type='radio' onClick=location.href='./?LED=ON' checked>LED ON <br> 
      <input type='radio' onClick=location.href='./?LED=OFF'>LED OFF </form>");
      sprintf(men_head, "<head><meta http-equiv='refresh' content=5; url=http://%d.%d.%d.%d/?LED=ON><title>ESP8266</title>", ip[0], ip[1], ip[2], ip[3]);
    } else if (linea1.indexOf("LED=OFF")>0) { //Buscamos un LED=OFF en la 1°Linea
      digitalWrite(2,LOW);
      sprintf(men_led, "<form method='GET' action=LED> <input type='radio' onClick=location.href='./?LED=ON'>LED ON <br> 
      <input type='radio' onClick=location.href='./?LED=OFF' checked>LED OFF </form>");
      sprintf(men_head, "<head><meta http-equiv='refresh' content=5; url=http://%d.%d.%d.%d/?LED=OFF><title>ESP8266</title>", ip[0], ip[1], ip[2], ip[3]);
    } else {
      digitalWrite(2,HIGH);
      sprintf(men_led, "<form method='GET' action=LED> <input type='radio' onClick=location.href='./?LED=ON' checked>LED ON <br> 
      <input type='radio' onClick=location.href='./?LED=OFF'>LED OFF </form>");
      sprintf(men_head, "<head><meta http-equiv='refresh' content=5; url=http://%d.%d.%d.%d/?LED=ON><title>ESP8266</title>", ip[0], ip[1], ip[2], ip[3]);
    }*/
    
    //digitalWrite(16, !digitalRead(16));
    
    if (linea1.indexOf("RED_ON")>0){
      digitalWrite(14, HIGH);
    } else if (linea1.indexOf("RED_OFF")>0) {
      digitalWrite(14, LOW);
    } else if (linea1.indexOf("BLUE_ON")>0) {
      digitalWrite(13, HIGH);
    } else if (linea1.indexOf("BLUE_OFF")>0) {
      digitalWrite(13, LOW);
    } else if (linea1.indexOf("GREEN_ON")>0) {
      digitalWrite(12, HIGH);
    } else if (linea1.indexOf("GREEN_OFF")>0) {
      digitalWrite(12, LOW);
    }
    
    if (digitalRead(14)==HIGH) {
      sprintf(men_red, "<input type='checkbox' onClick=location.href='./?RED_OFF' checked> <font color='RED'> RED </font>");
    } else {
      sprintf(men_red, "<input type='checkbox' onClick=location.href='./?RED_ON'> <font color='RED'> RED </font>");
    }
    if (digitalRead(13)==HIGH) {
      sprintf(men_blue, "<br><input type='checkbox' onClick=location.href='./?BLUE_OFF' checked> <font color='BLUE'> BLUE </font>");
    } else {
      sprintf(men_blue, "<br><input type='checkbox' onClick=location.href='./?BLUE_ON'> <font color='BLUE'> BLUE </font>");
    }
    if (digitalRead(12)==HIGH) {
      sprintf(men_green, "<br><input type='checkbox' onClick=location.href='./?GREEN_OFF' checked> <font color='GREEN'> GREEN </font> </form>");
    } else {
      sprintf(men_green, "<br><input type='checkbox' onClick=location.href='./?GREEN_ON'> <font color='GREEN'> GREEN </font> </form>");
    }
    client.flush();
    
    respuesta(client);
    
    delay(1);
  }
}

void respuesta(WiFiClient client){
    sprintf(men_head, "<head><meta http-equiv='refresh' content=30; url=http://%d.%d.%d.%d/><meta charset='ANSI'><title>ESP8266</title>", ip[0], ip[1], ip[2], ip[3]);
                
    //Serial.println("Enviando respuesta...");   
    //Encabezado http    
    client.println("HTTP/1.1 200 OK");
    client.println("Content-Type: text/html");
    client.println("Connection: close");// La conexión se cierra después de finalizar de la respuesta
    client.println();
    //Pagina html  para en el navegador
    client.println("<!DOCTYPE HTML>");
    client.println("<html>");
    client.println(men_head);
    client.println("<body>");
    client.println("<div style='text-align:center;'>");
    client.println("<h1>ESP8266 server</h1>");
    client.println("<br>");
    client.println("<form method='GET'>");
    client.println(men_red);
    client.println(men_blue);
    client.println(men_green);
    client.println("<br><br><br>");
    client.println(men_temp);
    client.println("<br>");
    client.println(men_hum);
    client.println("<br>");
    client.println("</div>");
    client.println("</body></html>");
}
