#ifndef functions.h
#define functions.h

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <DHT.h>
 
#define DHTTYPE DHT11 // DHT 11
#define dht_dpin 5 //GPIO5


DHT dht(dht_dpin, DHTTYPE);

void temp_hum(void);

void clienteWeb(void);

void servidor(void);
void respuesta(WiFiClient client);

#endif
